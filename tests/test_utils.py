from mixer.backend.flask import mixer
from flaskr.models import User, AuthToken, History
from flaskr import utils
from . import app, db, password


def test_create_user(app, db):
    """Test that users can be correctly created."""

    with app.app_context():
        # test successfully creating a new user
        user = utils.create_user("username", "password")
        assert user is not None
        assert user.username == "username"
        assert user.check_pw("password") is True

        # test failing because a user already exists
        user = utils.create_user("username", "password")
        assert user is None


def test_add_history(app, db, password):
    """Test that adding a record to the history works as expected."""

    with app.app_context():
        user = mixer.blend(User, password=password)
        token = mixer.blend(AuthToken, user=user.id).token
        url = "/reverse"
        input = "input"
        output = "output"

        assert utils.add_history(token, url, input, output) is None  # doesn't return


def test_authenticate_user(app, db, password):
    """Test that users can be correctly authenticated and granted a token."""

    with app.app_context():
        user = mixer.blend(User, password=password)

        # test that a token is created successfully
        tok = utils.authenticate_user(user.username, "password")
        assert tok is not None

        # test that a token is returned correctly when it already exists.
        new_tok = utils.authenticate_user(user.username, "password")
        assert new_tok is not None and tok == new_tok

        # test that we get nothing if the user couldn't be authenticated
        assert utils.authenticate_user(user.username, "random_other_password") is None


def test_user_exists(app, db):
    """Test that the user_exists function works correctly."""

    with app.app_context():
        user = mixer.blend(User)
        assert True == utils.user_exists(user.username)


def test_token_exists(app, db, password):
    """Test that the token_exists function works correctly."""

    with app.app_context():
        user = mixer.blend(User, password=password)
        token = mixer.blend(AuthToken, user=user.id)

        assert True == utils.token_exists(token.token)


def test_get_user_from_token(app, db, password):
    """Test that the correct user is fetched from authtokens."""

    with app.app_context():
        user = mixer.blend(User, password=password)
        token = mixer.blend(AuthToken, user=user.id)

        assert utils.get_user_from_token(token.token) == user


def test_get_serialized_history(app, db, password):
    """Test that the correct user's history is correctly returned to them on request."""

    with app.app_context():
        user = mixer.blend(User, password=password)
        records = mixer.cycle(13).blend(History, user=user.id)

        assert len(utils.get_serialized_history(user)) == 13
